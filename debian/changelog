kanboard (1.2.38+ds-1~1.gbp446677) UNRELEASED; urgency=medium

  ** SNAPSHOT build @44667786e7c01e6fc558f163b6d9ce73c8160ca1 **

  [ Athos Ribeiro ]
  * Team upload.
  * bump Standards-Version to 4.6.2
  * add support for phpunit 10 (Closes: #1039751):
    - remove deprecated phpunit verbose option
    - comply with phpunit 10 removals and deprecations

  [ Joseph Nahmias ]
  * New upstream version 1.2.38+ds
  * refresh patches

 -- Joseph Nahmias <jello@debian.org>  Wed, 30 Oct 2024 00:01:40 -0400

kanboard (1.2.31+ds2-1) unstable; urgency=medium

  * New upstream version 1.2.31+ds2
    + exclude upstream's .gitattributes to fix salsa CI pipeline
  * add missing dep on libjs-jquery-ui (Closes: #1040558)
  * salsa-ci: do not build on i386; 32bit is unsupported
  * drop unused lintian source override

 -- Joseph Nahmias <jello@debian.org>  Sat, 08 Jul 2023 23:39:58 -0400

kanboard (1.2.31+ds-1) unstable; urgency=medium

  * New upstream version 1.2.31+ds
    - CVE-2023-36813: Avoid potential SQL injections without breaking
      compatibility with plugins
      https://github.com/kanboard/kanboard/security/advisories/GHSA-9gvq-78jp-jxcx
      (Closes: #1040265)
  * drop patches merged upstream; refresh others

 -- Joseph Nahmias <jello@debian.org>  Mon, 03 Jul 2023 20:54:54 -0400

kanboard (1.2.30+ds-1) unstable; urgency=medium

  * New upstream version 1.2.30+ds
  * drop/refresh patches as needed
  * fix PHPUnit deprecation warnings
  * mark patches as Fowarded upstream

 -- Joseph Nahmias <jello@debian.org>  Sat, 01 Jul 2023 23:10:51 -0400

kanboard (1.2.26+ds-4) unstable; urgency=medium

  * backport security fixes from kanboard v1.2.30
     > CVE-2023-33956: Parameter based Indirect Object Referencing leading
       to private file exposure
     > CVE-2023-33968: Missing access control allows user to move and
       duplicate tasks to any project in the software
     > CVE-2023-33969: Stored XSS in the Task External Link Functionality
     > CVE-2023-33970: Missing access control in internal task links feature
    (Closes: #1037167)

 -- Joseph Nahmias <jello@debian.org>  Wed, 07 Jun 2023 20:45:40 -0400

kanboard (1.2.26+ds-3) unstable; urgency=medium

  * backport fix for CVE-2023-32685 from kanboard v1.2.29
    https://github.com/kanboard/kanboard/security/advisories/GHSA-hjmw-gm82-r4gv
    Based on upstream commits 26b6eeb & c9c1872. (Closes: #1036874)

 -- Joseph Nahmias <jello@debian.org>  Sun, 28 May 2023 21:42:46 -0400

kanboard (1.2.26+ds-2) unstable; urgency=medium

  * properly test for lighty-enable-mod.
    This fixes a bug in how the postinst/prerm maint scripts check whether
    to enable kanboard for lighttpd, which caused it to fail when lighttpd
    was not installed. (Closes: #1035598)
  * adapt some more areas to the new Symfony EventDispatcher API
    fix a couple of spots where we missed updating to the new dispatch() API:
    - standard db-based Auth
    - jsonrpc Auth
  * do not redirect access to Kanboard's JSONRPC API.
    It uses its own authentication and shouldn't be bounced to the standard
    login page.
  * add autopkgtest to ensure Kanboard JSONRPC API (minimally) works
  * add apache install autopkgtest
  * test(jsonrpc): make curl report errors in a cleaner way
  * test(jsonrpc): add php-fpm as test dep

 -- Joseph Nahmias <jello@debian.org>  Tue, 16 May 2023 22:49:38 -0400

kanboard (1.2.26+ds-1) unstable; urgency=medium

  * [1f43019] New upstream version 1.2.26+ds
  * [e0916f0] d/patches: refresh and drop upstream merges
  * [8a5ade4] exclude 32bit archs from autopkgtest

 -- Joseph Nahmias <jello@debian.org>  Sat, 14 Jan 2023 19:54:15 -0500

kanboard (1.2.25+ds-3) unstable; urgency=medium

  * [b4fa823] a 64-bit arch is required for using/testing kanboard, not building
  * [dac145e] version build/test dep on pkg-php-tools (>= 1.41)
    ensures phpabtpl is available when backporting

 -- Joseph Nahmias <jello@debian.org>  Thu, 12 Jan 2023 11:19:30 -0500

kanboard (1.2.25+ds-2) unstable; urgency=medium

  * [887e6ff] only build on 64bit architectures
    upstream does not support 32bit and tests for y2038 rollover in times
    stored as a 32-bit int in PHP.  See testDueDateYear2038TimestampBug()
    in tests/units/Model/TaskCreationModelTest.php
  * [c871072] clean additional generated css files
  * [e0aa317] fix test failures with php8.2

 -- Joseph Nahmias <jello@debian.org>  Thu, 12 Jan 2023 00:05:46 -0500

kanboard (1.2.25+ds-1) unstable; urgency=medium

  * [6e31cb2] New upstream version 1.2.25+ds
    - includes upstream fix for using assertEqualsWithDelta()
      which fixes a FTBFS (Closes: #1021906)
  * [47a509b] drop patch merged upstream; refresh remaining
  * [cf1db16] save kanboard logfile as autopkgtest artifact
  * [53c46eb] put kanboard file cache in /var/cache
  * [0784b92] install top-level .htaccess as a conffile
  * [378f68b] ignore kanboard unittest output during build

 -- Joseph Nahmias <jello@debian.org>  Thu, 17 Nov 2022 11:43:52 -0500

kanboard (1.2.23+ds-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to rebuild with debhelper 13.10.

 -- Michael Biebl <biebl@debian.org>  Sat, 15 Oct 2022 12:12:18 +0200

kanboard (1.2.23+ds-1) unstable; urgency=medium

  * [a9bdcb4] d/copyright: PHPQRCode/Autoloader.php is under LGPL-2.1+,
    not LGPL-3.0+
    Thanks to Thorsten Alteholz
  * [53048a2] New upstream version 1.2.23+ds
  * [a28a4a4] refresh patches
  * [b3e45f0] add missing License for LGPL-2.1

 -- Joseph Nahmias <jello@debian.org>  Mon, 12 Sep 2022 21:15:58 -0400

kanboard (1.2.22+ds-1) unstable; urgency=medium

  * Initial release (Closes: #790814)
  * [2b6e9e9] add smoke test
  * [8501957] run upstream unit tests as autopkgtest
  * [7717e06] loosen php module version requirements
  * [ca5932b] clear executable-bit where unneeded
  * [417628b] do not install docs for vendored libs
  * [fbb713e] override lintian false positives
  * [da0c549] write package version to a file so it can be displayed
              to the user
  * [c619475] fix location to read app version
  * [09179f3] unmangle version for plugin compatibility testing
  * [28b2967] fix unit test that assumes tested version is git master
  * [e76932b] add gbp config
  * [622ade0] enable salsa CI
  * [249636c] run cronjob as www-data instead of root
  * [6586c9e] add systemd timer for daily maintenance as suggested by lintian
  * [1c63928] skip cronjob if systemd is active
  * [f444b22] add DEP3 headers to patches
  * [a93f835] generate manpage using help2man
  * [a42d3b3] create version.txt earlier so it is available during
              manpage generation
  * [de44deb] drop README.Debian

 -- Joseph Nahmias <jello@debian.org>  Sun, 24 Jul 2022 10:39:13 -0400
